/*
  Command 0
  Detect a arduino loaded with this software
  Syntax : cmd
  e.g. 0

  Command 1
  Start communication with a RFID in the field
  1

  Command 50
  rc522 firmware and digital self test. After this call we have to unplug and replug.
*/

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_PN532.h>

// These are used for I2C connections
#define PN532_IRQ     2
#define PN532_RESET   0


Adafruit_PN532 nfc(PN532_IRQ,PN532_RESET);

/* To enable/Disable debugging messages during development.
  Set this to 0 for production release.
*/
#define DEBUG 0
#define MFRC522_PICC_CMD_MF_AUTH_KEY_A  0
#define MFRC522_PICC_CMD_MF_AUTH_KEY_B  1

#define STATUS_WRITE_TO_SECTOR_TRAILER      30
#define STATUS_INVALID_ACCESS_RIGHT         31
#define STATUS_SECTOR_TRAILER_NOT_WRITABLE  32
#define STATUS_KEYA_NOT_WRITABLE            33
#define STATUS_KEYB_NOT_WRITABLE            34
#define STATUS_NO_CARD_FOUND                35
#define STATUS_UNKNOWN_COMMAND              36
#define STATUS_SELF_TEST_FAILED             37
#define STATUS_AUTH_FAILED                  38

// These are MFRC522 error codes
#define MFRC522_STATUS_OK               1 // Success
#define MFRC522_STATUS_ERROR            2  // Error in communication
#define MFRC522_STATUS_COLLISION        3  // Collission detected
#define MFRC522_STATUS_TIMEOUT          4  // Timeout in communication.
#define MFRC522_STATUS_NO_ROOM          5  // A buffer is not big enough.
#define MFRC522_STATUS_INTERNAL_ERROR   6  // Internal error in the code. Should not happen ;-)
#define MFRC522_STATUS_INVALID          7 // Invalid argument.
#define MFRC522_STATUS_CRC_WRONG        8  // The CRC_A does not match
#define MFRC522_STATUS_MIFARE_NACK      9   // A MIFARE PICC responded with NAK.

byte piccType;

/* Commands from serial monitor */
char command[100];
char cmdChar;
boolean cmdFound = false;
byte pos;
byte cmdLength = 0;
int cmd;

/* command ouput to indicate success or failure */
const char error[] = "ERROR";
const char ok[] = "OK";

/* Variables used to access sectors, block, sector trailer etc. */
byte sector;
byte block;
byte trailerBlock;
byte blockAddr;
byte keyA[6];
byte keyB[6];
byte value;
byte mfrc_status;
byte buffer[18];
byte byteCount;
long valueBlockData;
boolean invertedError;
byte g[4]; // Access bits for block 0,1,2 and 3(Sector Trailer)
byte newAccessRights;
byte firmareVersion;
uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
uint8_t uidLength;
// Length of the UID (4 or 7 bytes depending on ISO14443A card type)
uint8_t sak;
boolean failed = false;

//UID compare array
uint8_t uidComp[10];

/*
 * Function prototypes issues with ide 1.6.8
 */

void pollCard();
void printArray(uint8_t uid[],uint8_t uidLength);
                
void setup() {
  Serial.begin(57600);
  // Start PN532
  nfc.begin();

  //Board details
  pn532VersionDetails();

  // Set the max number of retry attempts to read from a card
  nfc.setPassiveActivationRetries(0x02);

  // configure board to read RFID tags
  nfc.SAMConfig();

  // Tell our buffer is more than 16 bytes
}

void loop() {
  pollCard();
}

/*
  - I noticed serial event is called once for every character.
  e.g. If I type 'a' in serial monitor, the serialevent is called once.
       If I type 'abcd' in serial monitor, the serial event is called four times.
  - Each command is terminated by \r and \n respectively.
*/
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    cmdChar = Serial.read();

    // When we detect \n it is complete command line
    if (cmdChar == '\n') {
      // Make valid C string by terminating with NULL
      command[cmdLength] = 0;
      cmdFound = true;
      cmdLength = 0;
      pos = 0;
    }
    else if (cmdChar != '\r') {
      // Ignore \r
      command[cmdLength] = cmdChar;
      cmdLength++;
    }
  }
}

/* Routines to extract sector and block in the command text.*/
/*
 - Find a postion in the command array so that we can run atoi()
 - We have check error condition.
*/
void nextCommandPos(char delimiter = ',') {
  while (command[pos] != delimiter) {
    pos++;
  }
  // Now skip the delimiter character.
  pos++;
}


/*
  - Read 16 byte size numbers in the command
  - The values are stored into buffer
*/
void readDataValues() {
  for (int i = 0; i <= 15; i++) {
    nextCommandPos();
    value = atoi(command + pos);
    buffer[i] = value;
  }
}

void debugMessages(char* func) {
  Serial.print(func);
  Serial.print(": cmd=");
  Serial.print(command);
  Serial.print(": result=");
}


void processCommand() {
  // Extract command from command string
  cmd = atoi(command);

  // Execute command
  switch (cmd) {
    case 0: // Ping command.
#if DEBUG
      debugMessages("ping()");
#endif
      Serial.print("MIFARE O.K.|");
      pn532VersionDetails();
      break;
    case 1: // Return UID of a RFID card.
#if DEBUG
      debugMessages("getRFIDCard()");
#endif
      getRFIDCard();
      break;
    case 31:
      // Read 4 bytes of data from a page
      readPage4Bytes();
      break;
    case 32:
      // Read 16 bytes of data from a page
      readPage16Bytes();
      break;
    case 33:
      // Write 4 bytes of data to a page
      writePage();
      break;
    case 34:
      // Write 4 bytes of data to number of pages
      writePages();
      break;
    case 40:
      // Detect type of card on the box
      detectCardType();
      break;
    case 45:
      // Auth UltraLight C using Arduino 3DES algorithm, read custom Keys.
      authenticate3des(false);
      break;
    case 46:
      // Auth UltraLight C using Arduino 3DES algorithm, using factory keys
      authenticate3des(true);
      break;
    case 47:
      // Write 4 bytes of data to a page using compatibility write command
      writeCompatibilityPage();
      break;
    case 48:
      // 3des authenticate using PC
      ulcAuthStep1();
      break;
    case 49:
      // 3des authenticate using PC
      ulcAuthStep2();
      break;
    case 51:
    // Write Keys
      ulcWriteKeys();
      break;  
    case 52:
    //Obtain balance from command
    //Check against balance on card
    //If less, send insufficient balance: KES blah 
    //If okay,update new balance, send balance updated: KES blah
    balanceCheckerUpdater();
      break;
    default: // Unknow command
      mfrc_status = STATUS_UNKNOWN_COMMAND;
      failure();
  }
  // get ready for next command
  cmdFound = false;
  //clear command
}


/*
  Command 1.
  - Look for a new RFID Card. If no card is detected we return ERROR.
  - If a card is found then we return UID is Hex value.
  - The card type is 4 - 1K classic, 5 - 4k Classic, 6 - Mifare Ultralight C
*/
/* Type of card.
  4 -> MIFARE 1KB
  5 -> MIFARE 4KB
  6 -> NFC Tags, NTAG203
*/
void getRFIDCard() {

  mfrc_status = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength, &sak);
  if (mfrc_status) {
    // Output UID of rfid card
    for (byte i = 0; i < uidLength; i++) {
      if (uid[i] < 0x10)
        Serial.print("0");
      Serial.print(uid[i], HEX);
    }
    // Output card type
    Serial.print(',');
    if (uidLength == 4) {
      piccType = 4;
      if (sak == 0x18) piccType = 5;
    } else {
      piccType = 6;
    }
    Serial.println(piccType);
  }
  else {
    mfrc_status = STATUS_NO_CARD_FOUND;
    failure();
  }
}

void pollCard() {
  boolean success;
  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
  bool auth_success;
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, &uid[0], &uidLength);
  
  if (success) {
    if(uidLength == 7)
    {
      //We have an Ultralight C card
      Serial.print("UID:");printArray(uid,uidLength);
      Serial.println();
      //Authenticate
      auth_success = authenticate3des(false);
  
      //If authenticated, write to card
      if(auth_success)
      {
        Serial.println("AUTH:401");

        //PROCESS COMMAND
        if(cmdFound)
        {
          processCommand();
        }
        
        Serial.println("END");
        delay(3000);
      }
      else
      {
         Serial.println("AUTH:404");        
         Serial.println("END");
      }
    }
    else
    {
      //break
    }
  }
  else
  {
    // PN532 probably timed out waiting for a card
    //Serial.println("Timed out waiting for a card");
  }
}

void printArray(uint8_t uid[],uint8_t uidLength)
{
  for (uint8_t i=0; i < uidLength; i++) 
    {
      Serial.print(uid[i], HEX); 
    }
}

void printCharArray(char uid[],uint8_t uidLength)
{
  for (uint8_t i=0; i < uidLength; i++) 
    {
      Serial.print(uid[i]); 
    }
}
/*
 * This function is used to detect the card second time.
 * Whenever we get error during card type detection, we keep the box in ready state
 */
void selectAgain() {
  mfrc_status = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength, &sak);
  if (mfrc_status != MFRC522_STATUS_OK) mfrc_status = STATUS_NO_CARD_FOUND;
#if DEBUG
  (mfrc_status == MFRC522_STATUS_OK) ? Serial.println("Select Again O.K.") : Serial.println("Select Again Failed");
#endif
}

void success(boolean sendOK) {
  if (sendOK) Serial.println(ok);
  failed = false;
}

void failure() {
  Serial.print(error);
  Serial.print(':');
  Serial.println(mfrc_status);
  failed = true;
}


/* Send byte array to Serial port */
void sendByteArray(byte data[], byte sz) {
  for (byte i = 0; i < sz; i++) {
    if (i != 0) Serial.print(',');
    if (data[i] < 16) Serial.print(0);
    Serial.print(data[i], HEX);
  }
  Serial.println();
}

/* Obtain firmware version of board */
void pn532VersionDetails() {
  uint32_t versiondata = nfc.getFirmwareVersion();
  if (!versiondata)
    Serial.println("PN5xx Detect Error");
  else {
    Serial.print("Found PN5"); Serial.print((versiondata >> 24) & 0xFF, HEX);
    Serial.print(" ver. "); Serial.print((versiondata >> 16) & 0xFF, DEC);
    Serial.print('.'); Serial.println((versiondata >> 8) & 0xFF, DEC);
  }
}


