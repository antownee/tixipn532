#include <DES.h>
#include <DES_config.h>
#include <printf.h>
DES des;

byte customKeys[] = {0x48,0x54,0x53,0x45,0x52,0x4f,0x46,0x45,
                     0x56,0x49,0x52,0x44,0x53,0x4c,0x4c,0x49, 
                     0x48,0x54,0x53,0x45,0x52,0x4f,0x46,0x45
                      };
                  
byte ulcKeys[] = {0,0,0,0,0,0,0,0,
                  0,0,0,0,0,0,0,0, 
                  0,0,0,0,0,0,0,0
                 };

byte factoryKeys[] = {0x49, 0x45, 0x4D, 0x4B, 0x41, 0x45, 0x52, 0x42,
                      0x21, 0x4E, 0x41, 0x43, 0x55, 0x4F, 0x59, 0x46,
                      0x49, 0x45, 0x4D, 0x4B, 0x41, 0x45, 0x52, 0x42
                     };

/*
 * Read keys from PC and authenticate.
 * Command 45
 * e.g. To authenticate using keys A7,A6,A5,A4,A3,A2,A1,B7,B6,B5,B4,B3,B2,B1 
 * use 45,168,167,166,165,164,163,162,161,184,183,182,181,180,179,178,177
 */
boolean authenticate3des(boolean useDefaultKeys) {
  if (useDefaultKeys) {
    for (int i = 0; i < 24; i++)
      ulcKeys[i] = factoryKeys[i];
  } else {
    // Read key used for auth
    //readKeyValues(ulcKeys, 16);
    
    // Copy key1 values to key3 as well
    //for (int i = 0; i <= 7; i++)
    //  ulcKeys[16 + i] = ulcKeys[i];

    for (int i = 0; i <= 24; i++)
      ulcKeys[i] = customKeys[i];
  }
  //Serial.print("Keys:");
  //sendByteArray(ulcKeys, 24);

  // If previous read/write failed then reselect the card
  if (failed) selectAgain();

  // Initiate Authentication, ULC return ek(RndB)
  byte ekRndB[] = {0, 0, 0, 0, 0, 0, 0, 0};
  nfc.finitepi_ulc_auth1(ekRndB);
  #if DEBUG
  Serial.print("ek(RndB):");
  sendByteArray(ekRndB, 8);
  #endif

  // Decrypt ekRndB
  byte tdesIV[] = {0, 0, 0, 0, 0, 0, 0, 0}; // We use 0 as iv
  byte RndB[8];
  des.set_IV(tdesIV);
  des.set_key(ulcKeys);
  #if DEBUG
  Serial.print("KEY:");
  sendByteArray(ulcKeys,16);  
  #endif
  des.set_size(8);
  des.tdesCbcDecipher(ekRndB, RndB);
  #if DEBUG
  Serial.print("RndB :");
  sendByteArray(RndB, 8);
  #endif

  //Regenerate 8 Byte random number
  byte RndA[8];
  for (int i = 0; i < 8; i++)
    RndA[i] = random(256);

  #if DEBUG
  Serial.print("RndA :");
  sendByteArray(RndA, 8);
  #endif

  // Shift RndB left
  byte RndB_[8];
  for (int i = 0; i <= 6; i++)
    RndB_[i] = RndB[i + 1];
  RndB_[7] = RndB[0];

  // Generate RndA||RndB'
  byte RndARndB_[16];
  for (int i = 0; i <= 7; i++) {
    RndARndB_[i] = RndA[i];
    RndARndB_[i + 8] = RndB_[i];
  }

  // Encrypt ek(RndA||RndB')
  byte ekRndARndB_[16];
  des.set_IV(ekRndB); // We use this as iv
  des.set_size(16);
  des.tdesCbcEncipher(RndARndB_, ekRndARndB_);
  #if DEBUG
  Serial.print("ek(RndA||RndB'):");
  sendByteArray(ekRndARndB_, 16);
  #endif

  // Initiate Authentication Step 2, ulc will return ek(RndA')
  byte ekRndA_[] = {0, 0, 0, 0, 0, 0, 0, 0};
  nfc.finitepi_ulc_auth2(ekRndARndB_, ekRndA_);
  #if DEBUG
  Serial.print("ek(RndA):");
  sendByteArray(ekRndA_, 8);
  #endif

  // Decrypt ekRndA_, Shift Right and extract RndA
  byte RndA_[8];
  des.set_IV(ekRndARndB_ + 8);
  des.set_size(8);
  des.tdesCbcDecipher(ekRndA_, RndA_);

  // Shift right
  byte B7 = RndA_[7];
  for (int i = 7; i > 0; i--)
    RndA_[i] = RndA_[i - 1];
  RndA_[0] = B7;
  #if DEBUG
  Serial.print("RndA Rcvd :");
  sendByteArray(RndA_, 8);
  #endif

  // Compare with our value
  boolean ok = true;
  for (int i = 0; i <= 7; i++) {
    if (RndA_[i] != RndA[i]) {
      ok = false;
      mfrc_status=STATUS_AUTH_FAILED;
      return ok;
      break;
    }
  }

  return ok;
  // Tell user is success or failure
  //(ok) ? success(true) :  failure();
}


/*
  - Read 16 key values from serial port
*/
void readKeyValues(byte arr[], byte sz) {
  for (int i = 0; i < sz; i++) {
    nextCommandPos();
    value = atoi(command + pos);
    arr[i] = value;
#if DEBUG
    Serial.print(arr[i], HEX); Serial.println(',');
#endif
  }
}

/*
 * Write 16 byte keys to page 44,45,46,47
 * command 51. - Factory Keys 52 - Custom Keys
 * e.g. to use keys A7,A6,A5,A4,A3,A2,A1,B7,B6,B5,B4,B3,B2,B1 
 * 51,161,162,163,164,165,166,167,168,177,178,179,180,181,182,183,184
 */
void ulcWriteKeys() {
  int idx1 = 8;
  int idx2 = 4;
  int idx3 = 16;
  int idx4 = 12;

  int pageIndex [] = {8,4,16,12};
  // Read 16 bytes keys into variable ulcKeys[]
  readKeyValues(ulcKeys, 16);

  for(int i = 44; i <= 47; i++)
  {
    int pi = pageIndex[i-44];
    ulcPageArranger(pi,i);
  }
  
  // Tell user is success or failure
  (mfrc_status == 1) ? success(true) :  failure();
}

/*
  *Page arranger
  *
  */

void ulcPageArranger(int idx,int page)
{
  idx--;
  clearBuffer();
  for (int i = 0; i < 4; i++) {
      buffer[i] = ulcKeys[idx];
      idx--;
    }
  Serial.print(page);  Serial.print(": ");
  sendByteArray(buffer,4);
  mfrc_status = nfc.ntag_CompWritePage(page,buffer);
  Serial.print(mfrc_status);Serial.print(" \n");
}

/*
 * 3DES authentication using .NET functions in PC
 * Command : 48
 * This command is the first step where a arduino sends a challenge command to ULC
 * All data received from ULC is passed to PC for decrypt
 */
void ulcAuthStep1() {
  byte ekRndB[] = {0, 0, 0, 0, 0, 0, 0, 0};

  // Initiate Authentication. ulc returns ek(RndB)
  nfc.finitepi_ulc_auth1(ekRndB);

  // Send ek(RndB) to PC
  sendByteArray(ekRndB, 8);
}

/*
 * 3DES authentication using .NET functions in PC
 * Command : 49
 * This command is the second step where PCD sends enciphered data to ULC again
 */
void ulcAuthStep2() {
  // Read ek(RndA||RndB') from PC
  byte ekRndARndB_[16];
  readKeyValues(ekRndARndB_, 16);

  // Initiate Authentication Step 2, send ekRndARndB_ to ulc. ulc return ek(RndA')
  byte ekRndA_[] = {0, 0, 0, 0, 0, 0, 0, 0};
  nfc.finitepi_ulc_auth2(ekRndARndB_, ekRndA_);

  // Send ek(RndA') to PC
  sendByteArray(ekRndA_, 8);
}

