byte pageNo;
unsigned int amount;
unsigned int balance;
byte mfrc_write_status;
byte buf[8];


void extractPage() {

  // Move passed the command
  nextCommandPos();

  // Get page number
  pageNo = atoi(command + pos);
#if DEBUG
  Serial.println("extractPage()");
  Serial.println("Page :"); Serial.println(pageNo);
#endif
}

void extractAmount() {

  // Move past the command
  nextCommandPos();

  // Get amount
  amount = atoi(command + pos);

#if DEBUG
  Serial.print("Amount :"); Serial.print(amount);
#endif
}


void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], HEX);
    }
}


/*
*
* Cmd : 52, amount
* Usage : 52, amount
*/
void balanceCheckerUpdater() {

  // Get amount from command line
  extractAmount();


  // Use MFRC library to read.
  mfrc_status = nfc.ntag_ReadPage(38, buffer);

  if (mfrc_status == 1) {
    //Obtain balance
    //Convert to int then compare to received amount
    String b = "";
    char str[8];
    for(int i= 0; i <2 ; i++)
    {
      if (buffer[i] < 0x10) b.concat(0);
      b.concat(String(buffer[i],HEX));
    }

    //Serial.println(b); b is the balance in hex
    b.toCharArray(str,8);
    unsigned int balance = strtol(str, NULL, 16);

    Serial.print("BALANCE:KES ");Serial.println(balance);

    //If less, send insufficient balance: KES blah 
    //If okay,update new balance, send balance updated: KES blah
    if(balance < amount)
    {
      Serial.println("INSUFFICIENT BALANCE");
    }
    else
    {
      //Write balance
      //Take the passed amount e.g 9999 and put it into a byte array of size 4
      unsigned int newBalance = balance - amount;
      
      char st[8];
      sprintf(st, "%04X", newBalance);
      //Serial.println(str);
      byte bufWriteData[4];
      //The difference between balance and amount is what we feed back. NOT AMOUNT
      //String(amount,HEX).toCharArray(tr, 4);

      //We are copying str to byte array because function write takes byte array not char array
      for (int i = 0; i < 4; i++){
        sscanf(&st[i * 2], "%2x", &bufWriteData[i]);
      }
      //dump_byte_array(bufWriteData, 4);
      
      mfrc_write_status = nfc.ntag_WritePage(38, bufWriteData);

      // Tell user is success or failure
      (mfrc_write_status == 1) ?  Serial.print("NEW BALANCE:KES "), Serial.println(newBalance) :  Serial.println("WRITE FAILED");

    }
  }
  else {
    failure();
  }
}


/*
* Even through there is no authentication in ultralight C, we have use StopCrypt
* once we read any data.
*
* Cmd : 31, readPage4Bytes
* Usage : 31,pageNo
*/
void readPage4Bytes() {

  // Get pageno from command line
  extractPage();

  // Use MFRC library to read.
  mfrc_status = nfc.ntag_ReadPage(pageNo, buffer);

  // Send first 4 bytes bytes as hex values, comma separated.
  if (mfrc_status == 1) {
    for (byte index = 0; index < 4; index++) {
      if (index > 0) Serial.print(',');
      if (buffer[index] < 0x10) Serial.print(0);
      Serial.print(buffer[index], HEX);
    }
    Serial.println();
    success(false);
  }
  else {
    failure();
  }
}

/*
 * Cmd : 32, readPage16Bytes
 * Usage : 32,pageNo
 */
void readPage16Bytes() {

  // Get pageno from command line
  extractPage();
// Use MFRC library to read. we read 4 pages
  mfrc_status = nfc.ntag_Read4Pages(pageNo, buffer);
  // Send 16 bytes as hex values, comma separated.
  if (mfrc_status == 1) {
    for (byte index = 0; index < 16; index++) {
      if (buffer[index] < 0x10) Serial.print(0);
      Serial.print(buffer[index], HEX);
      if (index != 15) Serial.print(',');
    }
    Serial.println();
    success(false);
  }
  else {
    failure();
  }
}

/*
 * Cmd : 33 writePage();
 * Usage : 33,PageNo,D0,D1,D2,D3
 * Writes 4 bytes to a specified page.
 * I am using compatibility write here.
*/
void writePage() {
  // Extract page no from the command.
  extractPage();

  // Extract 4 integers from the command.
  readPageDataValues();

  // Use MFRC library to write.
  mfrc_status = nfc.ntag_WritePage(pageNo, buffer);
  //mfrc_status = nfc.ntag_CompWritePage(pageNo, buffer);

  // Tell user is success or failure
  (mfrc_status == 1) ?  Serial.println("WRITE OK") :  Serial.println("WRITE FAILED");
}

void clearBuffer() {
  for (int i = 0; i < 18; i++)
    buffer[i] = 0;
}

/*
 * Cmd : 47 writeCompatibilityPage();
 * Usage : 47,PageNo,D0,D1,D2,D3
 * Writes 4 bytes to a specified page.
*/
void writeCompatibilityPage() {
  // Extract page no from the command.
  extractPage();

  // Extract 4 integers from the command.
  clearBuffer();
  readPageDataValues();

  // Use MFRC library to write.
  mfrc_status = nfc.ntag_CompWritePage(pageNo, buffer);

  // Tell user is success or failure
  (mfrc_status == 1) ? success(true) :  failure();
}

/*
  - cmd,pageFrom,PageTov1,v2,v3,v4
*/
void writePages() {
  byte pageFrom, pageTo;

  // Extract From pageno from the command.
  extractPage();
  pageFrom = pageNo;

  // Extract To pageno from the command
  extractPage();
  pageTo = pageNo;

  // Extract 4 integers from the command.
  readPageDataValues();

  for (byte i = pageFrom; i <= pageTo; i++) {
    // Use MFRC library to write.
    mfrc_status = nfc.ntag_WritePage(i, buffer);

    // Just flash Green light
    success(false);

    // Tell user of success or failure
    if (mfrc_status != 1) break;
  }
  // Tell user is success or failure
  (mfrc_status == 1) ? success(true) :  failure();
}

void sendREQA_WUPA() {
}

void writeStaticLock() {
}
void writeDynamicLock() {
}
void writeOTP() {
}

/*
  Used for
  - To read 4 bytes of page data to write.
*/
void readPageDataValues() {
  for (int i = 0; i <= 3; i++) {
    nextCommandPos();
    value = atoi(command + pos);
    buffer[i] = value;
  }
}

/***
 *  Card Type ,   product_type, storage_size
 *  Error             0,0
 *  UltraLight        1,0
 *  Ntag203           2,0
 *  UltraLight EV1    3,x
 *  NTAG21x           4,x
 *    4,11  -> NTAG210
 *    4,14  -> NTAG212
 *    4,15  -> NTAG213
 *    4,17  -> NTAG215
 *    4,19  -> NTAG216
 *  Ultralight C      5,x
 */
void detectCardType() {
  byte product_type = 0;
  byte storage_size = 0;
  byte versionData[10] = {0};
#if DEBUG
  Serial.print("detectCardType");
#endif
  mfrc_status = nfc.NFC_GET_VERSION(versionData);
  if (mfrc_status != MFRC522_STATUS_OK) {
    // This card does not support this command. so, this must be either Ultralight or NTAG 203
    selectAgain();
    if (mfrc_status == MFRC522_STATUS_OK) {
      // Read Page 0
      mfrc_status = nfc.ntag_ReadPage(0, buffer);
      if (mfrc_status == MFRC522_STATUS_OK) {
        // It is an ultralight kind, it could be UltraLight, NTAG203 or Ultralight C
        product_type = 1;
        // If we can read page no 16, then it is NTAG 203 or UltraLight C
        mfrc_status = nfc.ntag_ReadPage(16, buffer);
        if (mfrc_status == MFRC522_STATUS_OK) {
          // It is NTAG203 or Ultra Light C
          product_type = 2;
          // If we can read page 42 it is Ultralight C
          mfrc_status = nfc.ntag_ReadPage(42, buffer);
          if (mfrc_status == MFRC522_STATUS_OK) {
            // It is Ultra Light C
            product_type = 5;
          }
        }
        // Keep it is selected if we failed trying to detect properly
        if (mfrc_status != MFRC522_STATUS_OK)
          selectAgain();
      }
    }
  } else {
    product_type = versionData[2];
    storage_size = versionData[6];
  }
  if (mfrc_status != MFRC522_STATUS_OK) product_type = 0;
  Serial.print(product_type);
  Serial.print(',');
  Serial.println(storage_size);
}

/**
 * Increment a counter
 * All counters can always be incremented and read without prior password verification
 */
void EV1IncCntr() {
  byte counterNo;
  long incrBy;
  // Get the counter number
  nextCommandPos();
  counterNo = atoi(command + pos);

  // Get Increment By
  nextCommandPos();
  incrBy = atol(command + pos);

  // Send command to EV1
  mfrc_status = nfc.NFC_INCR_CNTx(counterNo, incrBy);

  // Tell user is success or failure
  (mfrc_status == MFRC522_STATUS_OK) ? success(true) :  failure();
}

/***
 * Read the value of specified counter.
 */
void EV1ReadCntr() {
  byte counterNo;
  long value;
  // Get the counter number
  nextCommandPos();
  counterNo = atoi(command + pos);

  // Send command to EV1
  mfrc_status = nfc.NFC_READ_CNTx(counterNo, &value);

  // Tell user is success or failure
  if (mfrc_status == MFRC522_STATUS_OK)
    Serial.println(value);
  else
    failure();
}

/***
 * Read the value of specified counter.
 */
void EV1TearingEvent() {
  byte counterNo;
  byte value;
  // Get the counter number
  nextCommandPos();
  counterNo = atoi(command + pos);

  // Send command to EV1
  mfrc_status = nfc.NFC_CHECK_TEARING_EVENTx(counterNo, &value);

  // Tell user is success or failure
  if (mfrc_status == MFRC522_STATUS_OK)
    Serial.println(value);
  else
    failure();
}

/**
 * Send VCSL command
 * Command : 44,iid=bytes[16],pcdcaps=bytes[4]
 */
void  EV1Vcsl() {
  byte iid[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
  byte pcdcaps[] = {20, 30, 40, 50};
  byte vctid;
  // Read 16 bytes of iid
  for (int i = 0; i < 16; i++) {
    nextCommandPos();
    iid[i] = atoi(command + pos);
  }
  // Read 4 bytes of pcdcaps
  for (int i = 0; i < 4; i++) {
    nextCommandPos();
    pcdcaps[i] = atoi(command + pos);
  }
  // Send command to EV1
  mfrc_status = nfc.NFC_VCSL(iid, pcdcaps, &vctid);
  // Tell user is success or failure
  if (mfrc_status == MFRC522_STATUS_OK)
    Serial.println(vctid);
  else
    failure();
}










